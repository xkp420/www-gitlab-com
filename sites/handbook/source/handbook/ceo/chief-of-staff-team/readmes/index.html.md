---
layout: markdown_page
title: "Chief of Staff Team to the CEO READMEs"
---
## Chief of Staff Team to the CEO READMEs

- [Stella's README (Chief of Staff to the CEO)](/handbook/ceo/chief-of-staff-team/readmes/streas/)
- [Ian Pedowitz's README (Director, Strategy and Operations)](https://gitlab.com/ipedowitz)
- [Darren Murph's README (Head of Remote)](/handbook/ceo/chief-of-staff-team/readmes/dmurph/)
- [Jessica Reeder's README (Senior Strategy and Operations Manager: Workplace)](/handbook/ceo/chief-of-staff-team/readmes/jessicareeder/)
